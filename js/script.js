var arrow = $('.arrow');
var currentSection = 0;
var $w = $(window);
var $subscribe = $('.signup input[type="submit"]');

// Hides or Shows nav in mobile
$('.burger').on('click', function() {
  $('nav').toggle(300);
});

//If mobile, hide nav on menu item click
$('nav a').on('click', function() {
  if ($w.width() < 716) {
    $('nav').hide(500);
  }
});

//Smooth Scrolling: https://css-tricks.com/snippets/jquery/smooth-scrolling/
$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});

//Make header get compact on scroll
//Watch the scrolling in the browser, and do this function
$w.scroll(function() {
  var $this = $(this);

  //If user has scrolled past 300px
  if ($this.scrollTop() > 300 && $w.width() > 716) {
    //animate the header so the margin-top is at 0px. Do it with a .1s delay
    $('header').css({
      height: '60px'
    });
    $('header img').css({
      width: '85px'
    });
    $('nav').css({
      marginTop: '-10px'
    });
    $('nav a').css({
      padding: '5px 0px'
    });
  } else if ($this.scrollTop() <= 300 && $w.width() > 716) {
   $('header').css({
      height: '110px'
    });
     $('header img').css({
      width: '190'
    });
     $('nav').css({
      marginTop: '0px'
    });
     $('nav a').css({
      padding: '20px 0px'
    });
  } else if ($this.scrollTop() >= 200 && $w.width() <= 716) {
    $('header').addClass('transparent');
    $('header img').addClass('hide-this');
  } else if ($this.scrollTop() < 200 && $w.width() <= 716) {
    $('header').removeClass('transparent');
    $('header img').removeClass('hide-this');
  }
});

//Handle arrow class and inversion
$w.scroll(function() {
  var contactTop = $(".contact-section").offset().top;
  var scrollTop = $w.scrollTop();

  if (scrollTop >= contactTop) {
    arrow.addClass('invert');
  } else {
    arrow.removeClass('invert');
  }

  if (scrollTop >= 251 && scrollTop < contactTop) {
    arrow.addClass('hide-this');
  } else {
    arrow.removeClass('hide-this');
  }
});


//handle arrow navigation
arrow.on('click', function() {
  var $this = $(this);

  if ($this.hasClass('invert')){
    $this.attr("href", "#main");
  } else {
    $this.attr("href", "#shop");
  }
});


//img hover states in footer
$('.fb a').hover(
  function() {
    $('.fb img').attr('src', 'img/social_facebook_hov.png');
  },
  function() {
    $('.fb img').attr('src', 'img/social_facebook.png');
  }
);

$('.em a').hover(
  function() {
    $('.em img').attr('src', 'img/email_hov.png');
  },
  function() {
    $('.em img').attr('src', 'img/email.png');
  }
);
